clear;
close all;

% physical parameters
layer_thickness = 29.8e-6; %unit: m
diameter = 15e-3; %unit: m
area = pi*(diameter/2)^2;
eps_0 = 8.854e-12; % vacuum permittivity, unit: F/m
eps_r = 2.9; % relative permittivity
expected_cap = area*eps_0*eps_r/layer_thickness;

target_freq = 100;
soft_bk_thresh = exp(-1); % soft breakdown threshold - at what pct of original capacitance is the device considered broken?

data_directory = 'G:\Shared drives\Woodlab Storage\Andy\Electrode characterization\2022.08.12 - CB contacts';
name = regexp(data_directory, filesep, 'split');
name = name(end-1);

cp_files = dir(fullfile(data_directory, '*-Cp*.dat'));
g_files = dir(fullfile(data_directory, '*-G*.dat'));

%% find capacitance and tand
cp_data = cell(length(cp_files), 1);
cp_targ = nan(length(cp_files), 1);
cutoff = nan(length(cp_files), 1);
tand_targ = nan(length(cp_files), 1);
measurement_voltage = [];
specimen_idx = NaN(size(cp_files));

for file_ind = 1:length(cp_files)
    name_string = regexp(cp_files(file_ind).name, '-', 'split');
    voltage_string = name_string{3};
    voltage_string = regexp(voltage_string, 'V', 'split');
    voltage_string = voltage_string{1};
    measurement_voltage(file_ind) = str2double(voltage_string);
    
    specimen_idx(file_ind) = str2double(name_string{1});
    
    % import the cap files
    file = fullfile(data_directory, cp_files(file_ind).name);
    cp_data{file_ind} = readmatrix(file);
    
    % extract relevant data
    freq = cp_data{file_ind}(:, 1);
    cp = cp_data{file_ind}(:, 2);
    tand = cp_data{file_ind}(:, 3);
    
    % find the cutoff freq
    max_idx = find(freq > 100, 1)-1; % only use the data from under 100 Hz for max cp calc
    cp_max = mean(cp(1:max_idx));
    if isempty(find(cp > .5*cp_max, 1, 'last'))
        cutoff = NaN;
    else
        cutoff(file_ind) = freq(find(cp > .5*cp_max, 1, 'last')); % use 50% (-3 dB criterion)
    end
    
    % find the closest frequency to target and output cp and tand at that
    % frequency
    [~, targ_idx] = min(abs(freq-target_freq));
    cp_targ(file_ind) = cp(targ_idx);
    tand_targ(file_ind) = tand(targ_idx);
end

%% find conductance (g)
g_data = cell(length(g_files), 1);
g_targ = nan(length(g_files), 1);

for file_ind = 1:length(g_files)
    % import the conductance files
    file = fullfile(data_directory, g_files(file_ind).name);
    g_data{file_ind} = readmatrix(file);
    
    % extract relevant data
    freq = g_data{file_ind}(:, 1);
    g = g_data{file_ind}(:, 2);
    
    % find the closest frequency to target and output cp and tand at that
    % frequency
    [~, targ_idx] = min(abs(freq-target_freq));
    g_targ(file_ind) = g(targ_idx);
end

%% organize and plot data

% determine all unique specimen indices
file_inds = unique(specimen_idx);
output_cp = NaN([length(file_inds), length(unique(measurement_voltage))]);
output_g = NaN([length(file_inds), length(unique(measurement_voltage))]);
output_cutoff = NaN([length(file_inds), length(unique(measurement_voltage))]);
bk_v = NaN(size(file_inds));

% for each unique index, find all rows with that index - plot results
figure, hold on;
t = tiledlayout('flow');

% sort all our results by measurement results instead of by alphabetical
for i=1:length(file_inds)
    k = find(specimen_idx == file_inds(i));
    % sort results by voltage
    [V, I] = sort(measurement_voltage(k));
    sorted_cp = cp_targ(k);
    sorted_cp = sorted_cp(I);
    sorted_cutoff = cutoff(k);
    sorted_cutoff = sorted_cutoff(I);
    sorted_g = g_targ(k);
    sorted_g = sorted_g(I);
    
    % find the soft breakdown voltage for each device
    cp_max = max(sorted_cp);
    bk_idx = find(sorted_cp > soft_bk_thresh*cp_max, 1, 'last'); 
    if bk_idx == length(V)
        interp_idx = [bk_idx-1 bk_idx];
    else
        interp_idx = [bk_idx bk_idx+1];
    end
    bk_v(i) = interp1(sorted_cp(interp_idx), V(interp_idx), soft_bk_thresh*cp_max);
    
    output_cp(i, :) = sorted_cp;
    output_g(i, :) = sorted_g;
    output_cutoff(i, :) = sorted_cutoff;
    
    ax = nexttile;
    yyaxis left;
    plot(V, sorted_cp*1e12, 'displayname', num2str(file_inds(i)));
    ylim([0 170]);
    if i==1
        ylabel('Capacitance, pF');
    end
    yyaxis right;
    plot(V, (1e-6)*sorted_g.^-1, 'displayname', num2str(file_inds(i)));
%     plot(V, sorted_cutoff);
    set(gca, 'YScale', 'log')
    ylim([1e-1 1e5]);
%     ylim([1e4 1e5]);
    set(gca, 'YScale', 'log')
    if i==1
        ylabel('Resistance, M\Omega');
%         ylabel('Cutoff frequency, Hz');
    end
    title(sprintf('Specimen %d', file_inds(i)));
    xlabel(t,'Voltage, V');
end

figure;
title('Mean values at target freq');
yyaxis left;
plot(V, mean(output_cp)*1e12, 'displayname', 'Mean capacitance');
ylim([0 170]);
ylabel('Capacitance, pF');
yyaxis right;
plot(V, (1e-6)*mean((output_g).^-1), 'displayname', 'Mean resistance');
set(gca, 'YScale', 'log')
ylim([1e-1 1e5]);
set(gca, 'YScale', 'log');
ylabel('Resistance, M\Omega');

figure;
title('Plot for paper - mean values');
plot(V/(layer_thickness*1e6), mean(output_cp)*1e12, 'displayname', 'Mean capacitance');
ylim([0 160]);
ylabel('Capacitance, pF');
xlabel('Electric field, V/\mu m');
xlim([0 55]);

% make a weibull plot
[scale, shape] = custom_weibull_plot(bk_v/(layer_thickness*1e6), [0 0 0], true);