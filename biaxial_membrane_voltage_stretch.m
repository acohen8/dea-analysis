close all;
clear;
H = 30e-6; % thickness of layer, in m
lam = linspace(1, 1.5, 1000);
mu = 210e3/3; % shear modulus = young's modulus / 3
sig = mu*(lam.^2 - lam.^-4); % Neo-hookean stress-stretch relation
eps_r = 3.2;
eps_0 = 8.854E-12;
eps = eps_r*eps_0;
V = H*lam.^-2.*sqrt(sig/eps); % voltage-stretch relation
plot(lam, V);
E_b = 36e6; % nominal breakdown field
V_b = E_b*H*lam.^-2;
hold on;
plot(lam, V_b, 'r');
xlabel('Stretch');
ylabel('Voltage, V');
legend('Voltage-stretch relationship', 'Breakdown voltage', 'location', 'southeast');
xlim([1 1.4]);
grid on;
set(gca,'fontsize', 12);