function bk_v = find_breakdown(bk_files, data_directory, breakdown_current, T_min)
    bk_data = cell(length(bk_files), 1);
    bk_v = nan(length(bk_files), 1);
    bk_idx = nan(length(bk_files), 1);
    max_current = -Inf;

    for file_ind = 1:length(bk_files)
        % import the breakdown files
        file = fullfile(data_directory, bk_files(file_ind).name);
        bk_data{file_ind} = readmatrix(file);

        % find the first point at which current exceeds breakdown threshold
        current = bk_data{file_ind}(:, 3);
        max_current = max([current; max_current]);
        T_samp = mean(diff(bk_data{file_ind}(:, 1)));
        bk_idx(file_ind) = find_last_quiescent(current, breakdown_current, T_min, T_samp);
        % get the voltage, but set to almost zero if slightly negative
        bk_v(file_ind) = max(bk_data{file_ind}(bk_idx(file_ind), 2), 0.01); % units = kV
    end
end