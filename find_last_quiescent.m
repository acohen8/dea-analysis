function last_n = find_last_quiescent(current, current_thresh, T_min, T_samp)
    %FIND_LAST_QUIESCENT search for quiescent periods in time-current data
    %   current: current data over time
    %   current thresh: current threshold for considering it breakdown
    %   T_min: minimum time in seconds required to be considered quiescent
    %   T_samp: sampling period of data

    % find quiescent periods
    Q = regionprops(current' < current_thresh); % flip it horizontal, treat like image

    % find the last one that is at least a certain length long
    Q_time = [Q.Area];
    Q_time = Q_time*T_samp; % convert sample numbers to actual times
    last_q = find(Q_time > T_min, 1, 'last'); % find last q-window
    if isempty(last_q)
        last_n = 1;
    else
        % find the final quiescent sample index
        last_n = Q(last_q).BoundingBox; % find last sample in last q-window
        last_n = last_n(1)+last_n(3)-0.5; % get the right side of the bounding box
    end
end



