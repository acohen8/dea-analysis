%% setup
clear;
close all;

% physical parameters
layer_thickness = 27.7e-6; %unit: m
diameter = 15e-3; %unit: m
area = pi*(diameter/2)^2;
eps_0 = 8.854e-12; % vacuum permittivity, unit: F/m
eps_r = 2.9; % relative permittivity

% analysis parameters
breakdown_current = 1; %unit: uA
T_min = .25; % minimum time for quiescent periods, sec
target_freq = 100; % capacitance freq to compare at, Hz

data_directory = 'G:\Shared drives\Woodlab Storage\Andy\Electrode characterization\2022.05.16 - treated centrifuge\Electrical';
name = regexp(data_directory, filesep, 'split');
name = name(end-1);
bk_files = dir(fullfile(data_directory, '*-Bk.txt'));
cp_files = dir(fullfile(data_directory, '*-Cp.dat'));
g_files = dir(fullfile(data_directory, '*-G.dat'));

% only take some of the specimens for plotting
specimen_subset = 1:11;

%% find breakdown strength
bk_v = find_breakdown(bk_files, data_directory, breakdown_current, T_min); % breakdown voltage, kV
bk_fields = 1000*bk_v/(layer_thickness*1e6); % breakdown field, V/um
bk_subset = bk_fields(specimen_subset);

figure();
[scale, shape] = custom_weibull_plot(bk_subset, [0 0 0], true);
text(40,-2.5, sprintf('Scale: %.3g\nShape: %.3g\nN = %d specimens\nH = %.1f um',...
            scale, shape, length(bk_subset), 1e6*layer_thickness));
title(name);

saveas(gcf, fullfile(data_directory, 'Breakdown weibull.fig'));
saveas(gcf, fullfile(data_directory, 'Breakdown weibull.png'));

%% find capacitance
cp_data = cell(length(cp_files), 1);
cp_targ = nan(length(cp_files), 1);
tand_targ = nan(length(cp_files), 1);

for file_ind = 1:length(cp_files)
    % import the cap files
    file = fullfile(data_directory, cp_files(file_ind).name);
    cp_data{file_ind} = readmatrix(file);
    
    % extract relevant data
    freq = cp_data{file_ind}(:, 1);
    cp = cp_data{file_ind}(:, 2);
    tand = cp_data{file_ind}(:, 3);
    
    % find the closest frequency to target and output cp and tand at that
    % frequency
    [~, targ_idx] = min(abs(freq-target_freq));
    cp_targ(file_ind) = cp(targ_idx);
    tand_targ(file_ind) = tand(targ_idx);
end

%% find conductance (g)
g_data = cell(length(g_files), 1);
g_targ = nan(length(g_files), 1);

for file_ind = 1:length(g_files)
    % import the conductance files
    file = fullfile(data_directory, cp_files(file_ind).name);
    g_data{file_ind} = readmatrix(file);
    
    % extract relevant data
    freq = g_data{file_ind}(:, 1);
    g = g_data{file_ind}(:, 2);
    
    % find the closest frequency to target and output cp and tand at that
    % frequency
    [~, targ_idx] = min(abs(freq-target_freq));
    g_targ(file_ind) = g(targ_idx);
end

%% plot relationship between bk_voltage and capacitance/tand/g
n_plots = ~isempty(cp_files)*2 + ~isempty(g_files);
f = figure;
f.Position = [f.Position(1)-150 f.Position(2) 1000 420];

if ~isempty(cp_files)
    subplot(1, n_plots, 1);
    % extract only the subset we are interested in
    cp_targ_subset = cp_targ(specimen_subset);
    tand_targ_subset = tand_targ(specimen_subset);
    plot(cp_targ_subset*1e12, bk_subset, '*');
    xlabel(sprintf('Capacitance at %.0f Hz, pF', target_freq));
    ylabel('Breakdown field, V/um');
    for i=1:length(specimen_subset)
        spec = specimen_subset(i);
        text(cp_targ_subset(i)*1e12+.5, bk_subset(i), string(spec), 'Color', '#0072BD');
    end
    
    subplot(1, n_plots, 2);
    semilogx(tand_targ_subset, bk_subset, '*');
    xlabel(sprintf('tand at %.0f Hz', target_freq));
    ylabel('Breakdown field, V/um');
    for i=1:length(specimen_subset)
        spec = specimen_subset(i);
        text(tand_targ_subset(i)+.001, bk_subset(i), string(spec), 'Color', '#0072BD');
    end
    sgtitle(name);
    
    g_targ_subset = g_targ(specimen_subset);
    if ~isempty(g_files)
        subplot(1, n_plots, 3);
        plot(g_targ_subset*1e12, bk_subset, '*');
        xlabel(sprintf('Conductance at %.0f Hz, pS', target_freq));
        ylabel('Breakdown field, V/um');
        
        for i=1:length(specimen_subset)
            spec = specimen_subset(i);
            text(g_targ_subset(i)*1e12+.5, bk_subset(i), string(spec), 'Color', '#0072BD');
        end
    end
    
    saveas(gcf, fullfile(data_directory, 'Breakdown vs electrical props.fig'));
    saveas(gcf, fullfile(data_directory, 'Breakdown vs electrical props.png'));
end

%% get the current curves of the min, max, and closest to 63.2% breakdown specimens
[~, min_idx] = min(bk_subset);
[~, max_idx] = max(bk_subset);
[~, closest_idx] = min(abs(bk_subset - scale));

figure(), hold on;
indices = [min_idx, closest_idx, max_idx];
for i=1:length(indices)
    idx = specimen_subset(indices(i));
    file = fullfile(data_directory, bk_files(idx).name);
    bk_data = readmatrix(file);
    time = bk_data(:, 1);
    current = bk_data(:, 3);
    % move up the current to make it positive
    current = current - min(current) + .01;
    plot(time, current);
end

legend('Min', 'Closest', 'Max');
ylabel('Current, uA');
xlabel('Time, s');
set(gca, 'YScale', 'log')