function [peak_force, trough_force, differential, preload] =...
    peakForce(time, force_lp, sig_freq)
    % peakForce: gets averaged peak and rough parameters from a time series
    % measurement of DEA's response to applied voltage
    % CREDIT
    % Andy Cohen, Harvard Microrobotics Lab
    % INPUTS
    % time: time series in seconds
    % force_lp: force data in mN, already lp-filtered at 2x applied signal
    % sig_freq: scalar frequency of signal in Hz
    % fs: scalar sampling rate of the setup in Hz 
    % OUTPUTS
    % peak_force: the average peak force measured
    % trough_force: the average force measured at troughs
    % differential: peak_force-trough_force, provided for convenience
    % preload: the preload force calculated from non-activated period
    
    
    %% smooth and extract preload
    % apply a low pass at 2x signal - no longer needed (done in initial data gathering)
%     force_lp = lowpass(force, sig_freq*2, fs); 
    % find indices corresponding to no applied signal
%     neutral_time = .01;
%     ind_neutral = find(time > neutral_time, 1) - 1;
%     % calc preload of actuator, in mN
%     preload = mean(force_lp(1:ind_neutral)); 
%     % subtract preload
%     force_adj = force_lp - preload;

    %% find peaks (and troughs)
    % figure out which time period to ignore for peak detection by looking for
    % where force first exceeds 20% of the max force output
%     peak_start = find((force_lp - preload) > (max(force_lp)-preload)*0.25, 1);
    % ignore the first 10% as it stabilizes
    peak_start = round(0.1*length(time));
    % look for peaks separated by at least 75% of the applied signal period
    [pks, loc_pks] = findpeaks(force_lp(peak_start:end), time(peak_start:end), 'MinPeakDistance', 1/sig_freq*0.75); 
    % look for troughs separated by at least 75% of the applied signal period
    [tro, loc_tro] = findpeaks(-force_lp(peak_start:end), time(peak_start:end), 'MinPeakDistance', 1/sig_freq*0.75); 
    tro = -tro;
    % translate locations back to original indices of full data set
    loc_pks = loc_pks + (peak_start - 1);
    loc_tro = loc_tro + (peak_start - 1);

    %% get outputs and print
    peak_force = mean(pks);
    trough_force = mean(tro);
    differential = peak_force - trough_force;
%     fprintf('Peak force: %.4g mN\n', peak_force);
%     fprintf('Trough force: %.4g mN\n', trough_force);
%     fprintf('Differential: %.4g mN\n', differential);
%     fprintf('Preload: %.4g mN\n', preload);
    
    %% plot
%     close all;
%     yyaxis left;
%     plot(time, voltage);
%     ylabel('Applied voltage, kV');
%     hold on;
%     yyaxis right;
%     ylabel('Force output, mN');
%     plot(time, force_lp);
end