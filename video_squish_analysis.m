clear;
close all;

%% import 
v = VideoReader('C:\Users\anc7644\Downloads\zoomed_short_freestanding_multilayer_demo.mp4');
frames = read(v,[1 160]);
og_framerate = 4000;
T = 1/og_framerate*1000; % time between samples, msec

num_frames = size(frames, 4);
times = (0:num_frames-1)*T;
x_range = 234:450;
y_range = 181:241;
widths = NaN(num_frames, 1);
heights = NaN(num_frames, 1);
overlay_vid = uint8(zeros(size(frames)));

%% get edges and measure heights
for i=1:num_frames
    % extract the current avg height and width
    frame = frames(:, :, :, i);
    [width, height, edges] = get_stack_dims(rgb2gray(frame), x_range, y_range);
    widths(i) = width;
    heights(i) = height;
    
    % overlay the edge detection onto the original frame
    overlay = frame;
    overlay(:, :, 1) = overlay(:, :, 1) + uint8(255*edges);
    overlay_vid(:, :, :, i) = overlay;
end

%% create video

% plot the heights and widths
figure('position', [1.6667   41.6667  638.6667  599.3333]);
subplot(2, 1, 2);
hold on;
yyaxis left;
ylabel('Vertical strain, %');
rest_height = max(heights);
vertical_strains = 100*(heights-rest_height)/rest_height;
plot(times, vertical_strains);
yyaxis right;
rest_width = min(widths);
ylabel('Horizontal strain, %');
horizontal_strains = 100*(widths-rest_width)/rest_width;
plot(times, horizontal_strains);
xlabel('Time, msec');

% set up the video writer
v_out = VideoWriter('test.avi');
v_out.FrameRate = 10;
open(v_out);

% adjust the zoom
zoom_range_x = 160:500;
zoom_range_y = 140:300; 


for i=1:num_frames
    % plot the image and edge detection overlay
    subplot(2, 1, 1);
    imshow(overlay_vid(zoom_range_y, zoom_range_x, :, i));
    % add markers to indicate current time on the graph
    subplot(2, 1, 2);
    yyaxis left;
    h1 = plot(times(i), vertical_strains(i), 'o', 'Color', [0, 0.4470, 0.7410]);
    yyaxis right;
    h2 = plot(times(i), horizontal_strains(i), 'o', 'Color', [0.8500, 0.3250, 0.0980]);
    % convert the figure to a frame and add it to the video
    frame = getframe(gcf);
    writeVideo(v_out, frame);
    % get rid of the previous markers to reset for the next frame
    delete(h1);
    delete(h2);
end

close(v_out);