clear;
data_directory = 'G:\Shared drives\Woodlab Storage\Andy\Electrode characterization\2022.08.12 - CB contacts';
% G:\Shared drives\Woodlab Storage\Andy\Electrode characterization\2022.08.01 - increased CNT density\After re-cutting connections
% G:\Shared drives\Woodlab Storage\Andy\Electrode characterization\2021.10.06 - New baseline\Batch 1 - new baseline\Breakdown and capacitance
% G:\Shared drives\Woodlab Storage\Andy\Electrode characterization\2022.08.12 - CB contacts
name = regexp(data_directory, filesep, 'split');
name = name(end-1);

cp_files = dir(fullfile(data_directory, '*-Cp-500V*.dat'));
cp_data = cell(length(cp_files), 1);

for file_ind = 1:length(cp_files)
    % import the cap files
    file = fullfile(data_directory, cp_files(file_ind).name);
    cp_data{file_ind} = readmatrix(file);
    
    % extract relevant data
    freq = cp_data{file_ind}(:, 1);
    cp = cp_data{file_ind}(:, 2);
    tand = cp_data{file_ind}(:, 3);
    
    % add to mean
    if file_ind == 1
        mean_cp_sweep = zeros(size(cp));
    end
    mean_cp_sweep = mean_cp_sweep + cp/length(cp_files);
end

loglog(freq, mean_cp_sweep*1e12, 'displayname', 'CB-CNT Hybrid');
xlabel('Frequency, Hz');
ylabel('Capacitance, pF');