%% setup and data import
clear;
close all;
folder = 'G:\Shared drives\Woodlab Storage\Andy\Electrode characterization\2022.01.24 - laminator 30 um samples\Topology';
search_string = '*4x laminator*';
file_extension = '.csv';
files = dir2(fullfile(folder, [search_string file_extension]));

peak_height = [];
p = [];
area = 0;

for fileID=1:length(files)
    height = csvread(fullfile(folder, files(fileID).name), 19, 1);

    % eliminate errant final column
    height = height(:, 1:end-1);
    % todo: automate resolution extraction - for now change manually
    resolution = .25; % microns/pixel
    area = area + size(height, 1)*size(height,2)*resolution^2 / 1e6; % in mm
    
    % get the peak heights for this image, in microns, along with their
    % positions (stored in p)
    [peak_height_temp, p_temp] = find_electrode_peaks(height, resolution);
    % append to the data for all images
    peak_height = [peak_height; peak_height_temp];
    p = [p; p_temp];
    
    if fileID == 1
        figure;
        imagesc(height);
        hold on;
        plot(p_temp(1:2:end),p_temp(2:2:end),'r+');
    end
end

%% data plot
figure;
parmHat = wblfit(peak_height);
scale = parmHat(1);
shape = parmHat(2);
h = probplot('weibull',peak_height);
Y = h(1).YData';
X = h(1).XData';

axis([1 10 -4 2]);
xticks([1 2 4 8]);
xlabel('Peak height (um)');
ylabel('Probability');
title( sprintf('Peaks in %s', strrep(search_string, '*', ' ')) );
grid on;
fig = gcf;
axObjs = fig.Children;

% find the 95th percentile peak
% labels = deblank(string(axObjs.YTickLabel));
% index95 = find(strcmp(labels, "0.95"), 1);
% y95 = axObjs.YTick(index95);
% height95 = interp1(Y, X, y95);
height95 = prctile(peak_height, 95);

% Output info into graph and command line
fprintf('Number of peaks per unit area (0.01 mm^2): %.3g \n', length(peak_height)/area/100)
fprintf('95th percentile peak: %.3g um \n', height95);
text(5,-2.5, sprintf('%.0f peaks/mm^2\n95th percentile: %.3g um\nShape: %.3g\nScale: %.3g\nN=%d images',...
    length(peak_height)/area, height95, shape, scale, length(files)));
% save figure
saveas(gcf, fullfile(folder, ['Peak weibull - ', strrep(search_string, '*', ' '), '.fig']));
saveas(gcf, fullfile(folder, ['Peak weibull - ', strrep(search_string, '*', ' '), '.png']));

figure;
[counts, edges] = histcounts(peak_height, 'BinWidth', .5);
% histogram(peak_height, 'binwidth', .5);
histogram('BinEdges', edges, 'BinCounts', counts/area);
axis([0 20 0 400]);
text(12, 60, sprintf('%.0f total peaks/mm^2\nN = %d images',...
    length(peak_height)/area, length(files)));
ylabel('Peaks/mm^2');
xlabel('Height of peaks (um)');
title( sprintf('Peaks in %s', strrep(search_string, '*', ' ')) );

% make inset for tail end of distribution
tail_thresh = 5;
axes('Position',[.6 .6875 .275 .2])
box on
histogram('BinEdges', edges, 'BinCounts', counts/area);
axis([tail_thresh 20 0 25]);
% find the total greater than tail threshold
text( 6, -8, sprintf('%.0f peaks/mm^2 > %.0f um',...
    nnz(peak_height > tail_thresh)/area, tail_thresh), 'fontsize', 9 );

% save figure
saveas(gcf, fullfile(folder, ['Peak histogram - ', strrep(search_string, '*', ' '), '.fig']));
saveas(gcf, fullfile(folder, ['Peak histogram - ', strrep(search_string, '*', ' '), '.png']));