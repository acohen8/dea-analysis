function analyzeFreqSweep(data_directory, search_string, do_plot, plot_type)
    % @param plot_type type of plot to display. 0 = linear, 1 = semilogx
    %% setup
    % make sure all functions from this package are accessible
%     addpath(pwd);

    % search string to use for identifying folders
    folders = dir2(fullfile(data_directory, search_string));

    % where to save output data
    outname = 'output';
    outfile = fullfile(data_directory, [outname '.xlsx']);
%     outpicPeakForce = [outname '_peakForce.png'];
    outpic = fullfile(data_directory, [outname '_force.png']);
    % clear existing output if necessary
    if isfile(outfile)
        delete(outfile);
    end
    
    % stop matlab from warning you when it adds new sheets to a workbook
    warning( 'off', 'MATLAB:xlswrite:AddSheet' );

    %% scrape data, analyze, and plot
    for sweep=1:length(folders)
        folder = folders(sweep).name;
        search = fullfile(data_directory, folder, '*.xlsx');
        files = dir2(search);
        for file_ind = 1:length(files)
            % file for a single applied signal frequency
            file = fullfile(data_directory, folder, files(file_ind).name);
            data(:, :) = xlsread(file);
            sig_freq = data(1, 1); % signal frequency
            fs = data(1,2); % data sampling rate
            time = data(:,3)/1000; 
            force = data(:, 4); 
            voltage = data(:,5); % currently not used, not synchronized with force, synchronized with current
            current = data(:,6); % currently not used, not synchronized with force, synchronized with voltage
            [f_pk, f_tr] = peakForce(time, force, sig_freq);
            f_dif = f_pk - f_tr;
            f_pre = (f_pk + f_tr)/2;
            force_out(file_ind, :) = [sig_freq, f_pk, f_tr, f_dif, f_pre];
        end
        % sort output by frequency
        force_out = sortrows(force_out);
        % output the force data for the frequency sweep
        headers = {'Frequency (Hz)', 'Peak Force (mN)', 'Trough Force (mN)', 'Differential (mN)', 'Preload (mN)'};
        writecell(headers,outfile,'Sheet',folder)
        writematrix(force_out,outfile,'Sheet',folder,'Range','A2');

    end
    % delete blank sheets
    xls_delete_sheets(outfile,[]);

    %% plot, label, and save figures
    if do_plot
        [~,sheets] = xlsfinfo(outfile);
        f_dif_fig = figure();
        for i = 1:length(sheets)
            % extract each sweep
            data = readmatrix(outfile, 'sheet', sheets{i});
            sig_freq = data(:, 1);
%             peak_forces = data(:, 2);
            diff_forces = data(:, 4);

            % plot
            figure(f_dif_fig);
            if plot_type == 0
                plot(sig_freq, diff_forces);
            elseif plot_type == 1
                loglog(sig_freq, diff_forces);
            end
            hold on;
        end

        % create and export force figure
        figure(f_dif_fig);
        legend(sheets);
        xlabel('Signal frequency, Hz');
        ylabel('Force output, mN');
        set(gca, 'Color', 'none'); % Sets axes background to transparent
        export_fig(outpic, '-transparent', '-m2.5');
        savefig(f_dif_fig, fullfile(data_directory, [outname '_force.fig'])); % .fig allows for later editing
    end
end