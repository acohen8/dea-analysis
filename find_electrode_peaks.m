function [peak_height, p] = find_electrode_peaks(height, resolution)
    %FIND_ELECTRODE_PEAKS find the peaks within a given height scan of the
    %electrode
    %   Detailed explanation goes here
    %% normalize the image b/t 0 and 1
    norm_height = mat2gray(height);
    max_height = max(height(:));
    mean_height = mean(height(:));
    min_height = min(height(:));

    %% define a gaussian filter
    char_radius = 2; % - characteristic radius of peak
    sigma = char_radius/resolution; % standard deviation 
    filt_size = 2*ceil(2*sigma)+1;
    filt = fspecial('gaussian', filt_size, sigma);

    %% define our expected peak size (and thus threshold)
    thres = mean_height+0.5; % threshold in microns
    thres = thres/(max_height-min_height)*(2^16 - 1); % convert to uint16
    edg = 3;
    res = 1;

    %% find the peaks
    p = FastPeakFind(norm_height, thres, filt, edg, res);

    peak_number = length(p)/2;

    for i = 1:peak_number
        peak_height(i) = height(p(2*i),p(2*i-1)) - mean_height;
    end
    % delete peaks less than zero
    neg_peaks = find(peak_height < 0);
    [p([2*neg_peaks 2*neg_peaks-1])] = [];
    peak_height(neg_peaks) = [];
    peak_height = peak_height'; % make it vertical for easier inspection
end

