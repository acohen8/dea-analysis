function [width, height, edge_img] = get_stack_dims(img, x_range, y_range)
    edge_img = edge(img, 'canny', .2);
    
    [~, tops] = max(edge_img, [], 1);
    [~, bottoms] = max(flip(edge_img, 1), [], 1);
    bottoms = size(edge_img, 1) - bottoms + 1;
    
    heights = bottoms(x_range) - tops(x_range);
    height = mean(heights);
    
    [~, lefts] = max(edge_img, [], 2);
    [~, rights] = max(flip(edge_img, 2), [], 2);
    rights = size(edge_img, 2) - rights + 1;
    
    widths = rights(y_range) - lefts(y_range);
    widths = rmoutliers(widths, 'mean'); % get rid of anything 3 std dev off the mean
    width = mean(widths);
    
    % keep only perimeter edges of interest
    edge_img = zeros(size(edge_img));
    for x=x_range(1):x_range(end)
        edge_img(tops(x), x) = 1;
        edge_img(bottoms(x), x) = 1;
    end
    
    for y=y_range(1):y_range(end)
        edge_img(y, lefts(y)) = 1;
        edge_img(y, rights(y)) = 1;
    end
end