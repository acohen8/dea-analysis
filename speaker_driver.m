close all;
clear;

%% set up the daq
% connection setup
d = daq("ni");
dev_ID = 'Dev1'; % DeviceID, from daqlist()
d.Rate = 4000; % default sampling rate = 1000 Hz

% pin setup
V_SET = 'ao0'; % voltage command pin
V_MON = 'ai0'; % monitoring voltage pin
I_MON = 'ai1'; % monitoring current pin

% channel setup
addinput(d, dev_ID, V_MON, 'Voltage');
addinput(d, dev_ID, I_MON, 'Voltage');
addoutput(d, dev_ID, V_SET, 'Voltage');

%% Useful constants
V_AMP_TO_V_DAQ = 1/200; % 10 V monitor voltage range/2000 V output range
V_DAQ_TO_V_AMP = 1/V_AMP_TO_V_DAQ;

V_DAQ_TO_I_AMP = (20e-3)/10; % 20 mA amp range/10 V monitor voltage range
MAX_V_AMP = 2000;

%% create signal/import audio file
signal_duration = 10;
samples = signal_duration*d.Rate;

[y, fs] = audioread('SevenNationArmy_Bass.wav');
% we only need the second channel
y = y(:, 2);
% resample the signal to match the data rate of the DAQ
y = resample(y, d.Rate, fs);
% only extract a certain duration of signal
y = y(1:samples);

% alternatively we create our own signal
t = linspace(0, signal_duration, samples)'; % need to make this column vector
% f = 500;
y = 500*(1+sin(2*pi*f*t));
% y = 700*(1+square(2*pi*f*t));
% f0 = 100;
% f1 = 2000;
% y = 500*(1+chirp(t, f0, signal_duration, f1, 'logarithmic'));

y(end) = 0; % make sure we do not leave the voltage on

%% adjust signal amplitude
audio_to_signal = MAX_V_AMP*max(y);
y = audio_to_signal*y;
volume = 1.5;
y = y*volume;
% clip anything too high or low
y = max(y, -MAX_V_AMP);
y = min(y, MAX_V_AMP);

%% feed the signal into the amplifier and monitor currents and voltages
% convert set voltages to the correct 10 V monitor scale
y = y*V_AMP_TO_V_DAQ;
disp('Starting data write');
data = readwrite(d, y); % TODO: match sampling rates and scaling
v_mon = data.Dev1_ai0;
i_mon = data.Dev1_ai1;
figure, plot(data.Time, v_mon*V_DAQ_TO_V_AMP);
ylabel('Voltage, V');
% i_mon = lowpass(i_mon, 5, d.Rate);
figure, plot(data.Time, i_mon*V_DAQ_TO_I_AMP*1e6); % convert to uA
ylabel('Current, uA');
xlabel('Time, s');