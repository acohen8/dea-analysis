%% Housekeeping
clear, close all;

%% Add required function paths
% utilities path
utility = 'C:\Users\anc7644\utility_functions';
% libraries for doing special excel operations
addpath([utility '\xls_delete_sheets']);
% libraries for better exporting nice figures
addpath([utility '\export_fig-master']);

%% Parameter choice
% turn on/off plotting
do_plot = 1;
% search string used for finding data folders - don't use '**'
search_string = '*un*';
% location in which to find the data and in which to place output
% can just copy full address in file explorer to easily get this
data_directory = 'G:\Shared drives\Woodlab Storage\Andy\AC-013-02';

%% Execute analysis
plot_type = 0; % 0 = linear, 1 = loglog
analyzeFreqSweep(data_directory, search_string, do_plot, plot_type);