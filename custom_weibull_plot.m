function [scale, shape] = custom_weibull_plot(data, color, parameter_display)
    data = sort(data);
    n = length(data);
    p = NaN(size(data));
    
    % get fit parameters
    parmHat = wblfit(data);
    scale = parmHat(1);
    shape = parmHat(2);
    
    % calculate quantiles
    for i=1:n
        p(i) = (i-.3)/(n+.4);
    end
    
    % create data markers
    y = log(-log(1-p));
    h = semilogx(data, y, 'x',...
        'MarkerSize', 10, 'LineWidth', 1.0, 'Color', color);
    
    % format the plot
    hold on;
    p_ticks = [5 10 25 63.2 90 99]';
    yticks( percentile_to_y(p_ticks) );
    labels = cellstr(num2str(p_ticks));
    labels{4} = '\bf\it 63.2'; % make the breakdown strength bold
    yticklabels(labels);
    ylim( [ percentile_to_y(p_ticks(1)), percentile_to_y(p_ticks(end)) ] );
    xlim([5 80]);
    xticks([5, 10, 20, 40, 60, 80]);
    grid on;
    set(gca, 'Fontsize', 16);
    xlabel(['Electric field (V/',char(181),'m)'],'Interpreter','tex');
    ylabel('Failure probability (%)');
    
    % add the trend line
    x = xlim;
    x = linspace(x(1), x(2));
    fit = 1-exp(-(x/scale).^shape);
    plot(x, percentile_to_y(100*fit), '--', 'Color', h.Color, 'LineWidth', 1.0);
    
    % draw beta value as slope
    y0 = percentile_to_y(8);
    x_equiv = exp((y0 + shape*log(scale))/shape);
    x0 = x_equiv + .8*log(x_equiv);
    w = .75*log(x_equiv);
    alpha = x0*exp(-y0/shape);
    yf = shape*log(x0+w)-shape*log(alpha);
    
    pgon = polyshape([x0 y0; x0+w y0; x0+w yf]);
    plot(pgon, 'FaceColor', h.Color, 'FaceAlpha', .25, 'EdgeColor', h.Color, 'LineWidth', .75);
    text(x0, y0, ['\beta = ' sprintf('%.2f', shape)], ...
        'VerticalAlignment', 'top', 'Fontsize', 14, 'Color', h.Color);
end

function y = percentile_to_y(percentile)
    y = log(-log(1-percentile/100));
end

function p = y_to_percentile(y)
    p = 100*(1-exp(-exp(y)));
end